#include "endGame.h"

EndGame::EndGame() 
{
	continueButton = new Button;

	SetButtonSize();

	continueButton->SetText("CONTINUE");
}
EndGame::~EndGame() 
{
	delete continueButton;
}

void EndGame::Input(SceneManager* sceneManager, Scenes scene)
{
	continueButton->ChangeSceneWhenButtonPressed(sceneManager, scene);	
}
void EndGame::Update(Game* game) 
{
	continueButton->ChangeColorWhenMouseOnButton(recButtonColor, textButtonColor, textButtonColor, recButtonColor);

	if (continueButton->ButtonPressed()) 
	{
		game->ResetGame();
	}

	Cursor();
}
void EndGame::Draw(Game* game)
{
	BeginDrawing();

	ClearBackground(creditsBackgroundColor);
	
	continueButton->DrawButton();
	game->DrawScore();

	DrawText("YOUR LIFE ENDS HERE", GetScreenWidth() / 4, GetScreenHeight() / 2, 40, RED);

	EndDrawing();
}

void EndGame::SetButtonSize()
{
	continueButton->SetStructureWidth(GetScreenWidth() / bigButtonWidthDivision);
	continueButton->SetStructureHeight(GetScreenHeight() / bigButtonHeightDivision);
	continueButton->SetStructureX((GetScreenWidth() / 2.0f) - (continueButton->GetStructure().width / 2.0f));
	continueButton->SetStructureY(continueButton->GetStructure().height * 6);
}
void EndGame::Cursor() 
{
	if (IsCursorHidden()) 
	{
		ShowCursor();
	}
}