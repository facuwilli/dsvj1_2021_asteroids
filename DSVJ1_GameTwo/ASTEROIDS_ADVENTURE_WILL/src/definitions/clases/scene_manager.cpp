#include "scene_manager.h"

SceneManager::SceneManager()
{
	SetActualScene(startingScene);
}
void SceneManager::SetActualScene(Scenes actualScene)
{
	this->actualScene = actualScene;
}
Scenes SceneManager::GetActualScene()
{
	return actualScene;
}