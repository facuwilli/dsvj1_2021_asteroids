#include "config.h"

Config::Config() 
{
	goMainMenuButton = new Button();

	SetButtonSize();

	goMainMenuButton->SetText("MENU");
}
Config::~Config() 
{
	delete goMainMenuButton;
}

void Config::Input(SceneManager* sceneManager, Scenes scene)
{
	goMainMenuButton->ChangeSceneWhenButtonPressed(sceneManager, scene);
}
void Config::Update() 
{
	goMainMenuButton->ChangeColorWhenMouseOnButton(recButtonColor, textButtonColor, textButtonColor, recButtonColor);
}
void Config::Draw() 
{
	BeginDrawing();

	ClearBackground(creditsBackgroundColor);
	
	goMainMenuButton->DrawButton();

	DrawText("Ops seems the dev was a bit lazy", 100, 200, 30, WHITE);

	EndDrawing();
}

void Config::SetButtonSize() 
{
	goMainMenuButton->SetStructureWidth(GetScreenWidth() / bigButtonWidthDivision);
	goMainMenuButton->SetStructureHeight(GetScreenHeight() / bigButtonHeightDivision);
	goMainMenuButton->SetStructureX((GetScreenWidth() / 2.0f) - (goMainMenuButton->GetStructure().width / 2.0f));
	goMainMenuButton->SetStructureY(goMainMenuButton->GetStructure().height * 6);
}