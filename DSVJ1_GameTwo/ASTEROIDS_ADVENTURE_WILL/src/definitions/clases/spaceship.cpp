#include "spaceship.h"

Spaceship::Spaceship() 
{	
	for (short i = 0; i < maxBullet; i++)
	{
		bullets[i] = NULL;
	}

	ResetSpaceship();
	SetSides(spaceshipSides);
	SetRadius(spaceshipRadius);			
	SetColor(spaceshipColor);		
	SetVelocity( {150.0f , 150.0f} );	
}
Spaceship::~Spaceship() 
{
	for (short i = 0; i < maxBullet; i++)
	{
		if (bullets != NULL) 
		{
			delete bullets[i];			
		}	
	}
}

void Spaceship::SetCenter(Vector2 center) 
{
	this->center = center;
}
void Spaceship::SetSides(int sides) 
{
	this->sides = sides;
}
void Spaceship::SetRadius(float radius) 
{
	this->radius = radius;
}
void Spaceship::SetRotation(float rotation) 
{
	this->rotation = rotation;
}
void Spaceship::SetColor(Color color) 
{
	this->color = color;
}
void Spaceship::SetStatus(SpaceshipStatus status)
{
	this->status = status;
}
void Spaceship::SetVelocity(Vector2 velocity) 
{
	this->velocity = velocity;
}
void Spaceship::SetDistance(Vector2 distance) 
{
	this->distance = distance;
}
void Spaceship::DeleteBullet(short index) 
{
	if (bullets[index] != NULL) 
	{
		delete bullets[index];
		bullets[index] = NULL;
	}
}
void Spaceship::SetLifes(short lifes) 
{
	this->lifes = lifes;
}

Vector2 Spaceship::GetCenter() 
{
	return center;
}
int Spaceship::GetSides() 
{
	return sides;
}
float Spaceship::GetRadius() 
{
	return radius;
}
float Spaceship::GetRotation() 
{
	return rotation;
}
Color Spaceship::GetColor() 
{
	return color;
}
SpaceshipStatus Spaceship::GetStatus()
{
	return status;
}
Vector2 Spaceship::GetVelocity()
{
	return velocity;
}
Vector2 Spaceship::GetDistance()
{
	return distance;
}
Bullet* Spaceship::GetBullet(short index)
{
	return bullets[index];
}
short Spaceship::GetLifes() 
{
	return lifes;
}

void Spaceship::Input()
{
	if (IsMouseButtonDown(moveSpaceshipButton)) 
	{
		status = SpaceshipStatus::moving;
	}
	else 
	{
		status = SpaceshipStatus::repose;
	}

	BulletsInput();
}
void Spaceship::Update()
{
	RotateSpaceshipDependingOnMousePosition();	
	UpdateDistance();
	MoveSpaceship();
	BulletsUpdate();
}
void Spaceship::Draw()
{
	DrawSpaceship();		
	BulletsDraw();	
}

void Spaceship::DrawSpaceship() 
{
	DrawPoly(GetCenter(), GetSides(), GetRadius(), GetRotation(), GetColor());	
}
void Spaceship::RotateSpaceshipDependingOnMousePosition()
{
	float sideOne = abs(center.x - static_cast<float>(GetMouseX()));
	float sideTwo = abs(center.y - static_cast<float>(GetMouseY()));
	double angle = atan(sideTwo / sideOne) * 180 / PI;

	if (GetMouseX() > static_cast<int>(center.x) && GetMouseY() < static_cast<int>(center.y))
	{
		SetRotation((90.0f - static_cast<float>(angle)) + 180.0f);
	}
	else if (GetMouseX() < static_cast<int>(center.x) && GetMouseY() < static_cast<int>(center.y))
	{
		SetRotation((-90.0f + static_cast<float>(angle)) + 180.0f);
	}
	else if (GetMouseX() > static_cast<int>(center.x) && GetMouseY() > static_cast<int>(center.y))
	{
		SetRotation((90.0f + static_cast<float>(angle)) + 180.0f);
	}
	else if (GetMouseX() < static_cast<int>(center.x) && GetMouseY() > static_cast<int>(center.y))
	{
		SetRotation((-90.0f - static_cast<float>(angle)) + 180.0f);
	}
	else if (static_cast<int>(center.y) == GetMouseY())
	{
		if (static_cast<int>(center.x) < GetMouseX())
		{
			SetRotation(90 + 180);
		}
		else if (static_cast<int>(center.x) > GetMouseX())
		{
			SetRotation(-90 + 180);
		}
	}
	else if (static_cast<int>(center.x) == GetMouseX() && static_cast<int>(center.y) < GetMouseY())
	{
		SetRotation(360);
	}
	else
	{
		SetRotation(spaceshipInitialRotation);
	}	
}
void Spaceship::MoveSpaceship()
{
	if (status == SpaceshipStatus::moving)
	{
		center.x += distance.x * GetVelocity().x * GetFrameTime();
		center.y += distance.y * GetVelocity().y * GetFrameTime();
	}
}
void Spaceship::ResetSpaceship() 
{
	SetCenter({ static_cast<float>(GetScreenWidth() / 2), static_cast<float>(GetScreenHeight() / 2) });
	SetRotation(spaceshipInitialRotation);	
	SetStatus(SpaceshipStatus::repose);
	SetLifes(maxLifes);

	ResetBullets();
}
void Spaceship::ResetSpaceshipWithSameLifes()
{
	SetCenter({ static_cast<float>(GetScreenWidth() / 2), static_cast<float>(GetScreenHeight() / 2) });
	SetRotation(spaceshipInitialRotation);
	SetStatus(SpaceshipStatus::repose);
	ResetBullets();
}
void Spaceship::UpdateDistance() 
{
	float distanceX = GetMousePosition().x - center.x;
	float distanceY = GetMousePosition().y - center.y;
	double module = sqrt(pow(distanceX,2) + pow(distanceY,2));
	distanceX /= static_cast<float>(module);
	distanceY /= static_cast<float>(module);

	SetDistance({ distanceX, distanceY });
}

void Spaceship::BulletsInput()
{
	if (IsMouseButtonPressed(shootBulletButton))
	{
		for (short i = 0; i < maxBullet; i++)
		{
			if (bullets[i] == NULL)
			{
				bullets[i] = new Bullet(GetCenter(), GetDistance());
				i = maxBullet;
			}
		}
	}
}
void Spaceship::BulletsUpdate()
{
	for (short i = 0; i < maxBullet; i++)
	{
		if (bullets[i] != NULL)
		{
			bullets[i]->MoveBullet();

			if (!bullets[i]->IsBulletInsideMap())
			{
				delete bullets[i];
				bullets[i] = NULL;
			}
		}
	}
}
void Spaceship::BulletsDraw()
{
	for (short i = 0; i < maxBullet; i++)
	{
		if (bullets[i] != NULL)
		{
			bullets[i]->DrawBullet();
		}
	}
}
void Spaceship::ResetBullets()
{
	for (short i = 0; i < maxBullet; i++)
	{
		DeleteBullet(i);
	}
}