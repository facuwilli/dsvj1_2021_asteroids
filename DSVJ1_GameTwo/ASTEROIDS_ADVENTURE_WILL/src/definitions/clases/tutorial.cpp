#include "tutorial.h"

Tutorial::Tutorial() 
{
	goMainMenuButton = new Button();

	SetButtonSize();

	goMainMenuButton->SetText("MENU");
}
Tutorial::~Tutorial() 
{
	delete goMainMenuButton;
}

void Tutorial::Input(SceneManager* sceneManager, Scenes scene) 
{
	goMainMenuButton->ChangeSceneWhenButtonPressed(sceneManager, scene);
}
void Tutorial::Update() 
{
	goMainMenuButton->ChangeColorWhenMouseOnButton(recButtonColor, textButtonColor, textButtonColor, recButtonColor);
}
void Tutorial::Draw() 
{
	BeginDrawing();

	ClearBackground(creditsBackgroundColor);

	DrawTutorial();
	goMainMenuButton->DrawButton();

	EndDrawing();
}

void Tutorial::SetButtonSize()
{
	goMainMenuButton->SetStructureWidth(GetScreenWidth() / bigButtonWidthDivision);
	goMainMenuButton->SetStructureHeight(GetScreenHeight() / bigButtonHeightDivision);
	goMainMenuButton->SetStructureX((GetScreenWidth() / 2.0f) - (goMainMenuButton->GetStructure().width / 2.0f));
	goMainMenuButton->SetStructureY(goMainMenuButton->GetStructure().height * 6);
}
void Tutorial::DrawTutorial()
{
	DrawText("TUTORIAl", (GetScreenWidth() / 2) - (creditsTitleFontSize * 4), (GetScreenHeight() / creditsTextSizeDivisions) * 2, creditsTitleFontSize, WHITE);
	DrawText("YOU HAVE TO DESTROY ASTEROIDS", (GetScreenWidth() / 20) * 2, (GetScreenHeight() / creditsTextSizeDivisions) * 5, creditsTextFontSize, WHITE);
	DrawText("LEFT CLICK TO MOVE", (GetScreenWidth() / 20) * 2, (GetScreenHeight() / creditsTextSizeDivisions) * 7, creditsTextFontSize, WHITE);
	DrawText("RIGHT CLICK TO SHOOT", (GetScreenWidth() / 20) * 2, (GetScreenHeight() / creditsTextSizeDivisions) * 8, creditsTextFontSize, WHITE);
}