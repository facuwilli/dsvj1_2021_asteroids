#include "menu.h"

Menu::Menu(const char* texts[5])
{	
	for (short i = 0; i < maxButtonsInMenu; i++)
	{
		buttons[i] = new Button();
		buttons[i]->SetText(texts[i]);
	}

	SetButtonsStructure();	
}
Menu::~Menu()
{
	for (short i = 0; i < maxButtonsInMenu; i++)
	{
		delete buttons[i];
	}
}

void Menu::SetButtonsStructure() 
{
	float width = static_cast<float>(GetScreenWidth()) / widthDivisionNum;
	short height = static_cast<short>(GetScreenHeight() / heightDivisionNum);	
	float xPos = (width * (widthDivisionNum / 2.0f)) - (width / 2.0f);
	float yPos = height * (heightDivisionNum / static_cast<float>(maxButtonsInMenu + 1));

	for (short i = 0; i < maxButtonsInMenu; i++)
	{
		buttons[i]->SetStructure(xPos, yPos * (i + 1), width, height);		
	}
}

void Menu::InputMenu(SceneManager* sceneManager, Scenes scenes[maxButtonsInMenu])
{
	for (short i = 0; i < maxButtonsInMenu; i++)
	{
		buttons[i]->ChangeSceneWhenButtonPressed(sceneManager, scenes[i]);
	}
}
void Menu::UpadateMenu(Color recButtonColor, Color textButtonColor) 
{
	for (short i = 0; i < maxButtonsInMenu; i++)
	{
		buttons[i]->ChangeColorWhenMouseOnButton(recButtonColor, textButtonColor, textButtonColor, recButtonColor);
	}

	Cursor();
}
void Menu::DrawMenu() 
{
	BeginDrawing();

	ClearBackground(mainMenubackgroundClearColor);

	for (short i = 0; i < maxButtonsInMenu; i++)
	{
		buttons[i]->DrawButton();		
	}

	EndDrawing();
}
void Menu::Cursor() 
{
	if (IsCursorHidden()) 
	{
		ShowCursor();
	}
}