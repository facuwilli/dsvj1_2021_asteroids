#include "bullet.h"

Bullet::Bullet(Vector2 position, Vector2 distance)
{
	SetPosition(position);
	SetDistance(distance);
	SetColor(bulletColor);
	SetVelocity({ 400.0f , 400.0f });
}

void Bullet::SetPosition(Vector2 position)
{
	this->position = position;
}
void Bullet::SetDistance(Vector2 distance) 
{
	this->distance = distance;
}
void Bullet::SetColor(Color color)
{
	this->color = color;
}
void Bullet::SetVelocity(Vector2 velocity) 
{
	this->velocity = velocity;
}

Vector2 Bullet::GetPosition()
{
	return position;
}
Vector2 Bullet::GetDistance() 
{
	return distance;
}
Color Bullet::GetColor() 
{
	return color;
}
Vector2 Bullet::GetVelocity() 
{
	return velocity;
}

void Bullet::DrawBullet() 
{
	DrawPixel(static_cast<int>(GetPosition().x), static_cast<int>(GetPosition().y), GetColor());
}
void Bullet::MoveBullet()
{
	position.x += GetDistance().x * velocity.x * GetFrameTime();
	position.y += GetDistance().y * velocity.y * GetFrameTime();
}
bool Bullet::IsBulletInsideMap()
{
	return (position.x > 1 && position.x < GetScreenWidth() && position.y > 1 && position.y < GetScreenHeight());
}