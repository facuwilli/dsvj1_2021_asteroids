#include "game.h"

Game::Game() 
{
	spaceship = new Spaceship();
	asteroidsSpawner = new AsteroidsSpawner();

	ResetScore();
}
Game::~Game()
{
	delete spaceship;
	delete asteroidsSpawner;
}

void Game::Input(SceneManager* sceneManager)
{
	PauseGame(sceneManager);
	spaceship->Input();
}
void Game::Update(SceneManager* sceneManager)
{
	spaceship->Update();
	asteroidsSpawner->Update();		
	AsteroidBulletCollision();
	SpaceshipAsteroidsCollision();
	LoseCondition(sceneManager);
	Cursor();
	RestrictTheMouseMaxReach();

	if (allAsteroidsDestroy()) 
	{
		asteroidsSpawner->DestroyAsteroids();
		asteroidsSpawner->CreateAsteroids(maxAsteroids);
		spaceship->ResetSpaceshipWithSameLifes();
	}
}
void Game::Draw()
{
	BeginDrawing();

	ClearBackground(gameBackgroundColor);
	
	asteroidsSpawner->Draw();
	spaceship->Draw();
	DrawCursorPoint();
	DrawScore();
	DrawUI();

	EndDrawing();
}
void Game::GameLoop(SceneManager* sceneManager)
{
	Input(sceneManager);
	Update(sceneManager);
	Draw();
}
void Game::ResetGame() 
{
	ResetScore();
	asteroidsSpawner->DestroyAsteroids();
	asteroidsSpawner->CreateAsteroids(maxAsteroids);
	spaceship->ResetSpaceship();
}

void Game::PauseGame(SceneManager* sceneManager)
{
	if (IsKeyPressed(pauseGameKey))
	{
		sceneManager->SetActualScene(Scenes::pauseMenu);
	}
}
void Game::AsteroidBulletCollision()
{	
	//For every asteroid and bullet that is not null evaluates if there is collision to destoy them
	for (short i = 0; i < maxAsteroids; i++)
	{
		if (asteroidsSpawner->GetAsteroid(i) != NULL) 
		{
			for (short j = 0; j < maxBullet; j++)
			{
				if (spaceship->GetBullet(j) != NULL && asteroidsSpawner->GetAsteroid(i) != NULL)
				{
					if (CheckCollisionPointCircle(spaceship->GetBullet(j)->GetPosition(), asteroidsSpawner->GetAsteroid(i)->GetCenter(), asteroidsSpawner->GetAsteroid(i)->GetRadius()))
					{
						asteroidsSpawner->DeleteAsteroid(i);
						spaceship->DeleteBullet(j);
						ScoreUp();
					}
				}
			}
		}		
	}	
}
void Game::ResetScore() 
{
	for (short i = 0; i < maxScore; i++) 
	{
		score[i] = zero;
	}
}
void Game::ScoreUp() 
{
	score[3] += 1;

	for (short i = maxScore-1; i >= 0; i--)
	{
		if (score[i] > nine && i > 0)
		{
			score[i] = zero;

			score[i - 1] += 1;						
		}
	}
}
void Game::DrawScore() 
{
	DrawText(score, (GetScreenWidth() / 2) - ((scoreTextSize/2) * (maxScore / 2)), GetScreenHeight() / scoreHeightDivision, scoreTextSize, WHITE);
}
void Game::DrawCursorPoint() 
{
	DrawPixel(GetMouseX(), GetMouseY(), cursorPixelColor);
}
void Game::SpaceshipAsteroidsCollision() 
{
	for (short i = 0; i < maxAsteroids; i++)
	{
		if (asteroidsSpawner->GetAsteroid(i) != NULL)
		{
			if (CheckCollisionCircles(spaceship->GetCenter(), spaceship->GetRadius(), asteroidsSpawner->GetAsteroid(i)->GetCenter(), asteroidsSpawner->GetAsteroid(i)->GetRadius()))
			{
				asteroidsSpawner->DeleteAsteroid(i);		
				spaceship->SetLifes(spaceship->GetLifes() - 1);
			}			
		}
	}
}
void Game::LoseCondition(SceneManager* sceneManager)
{
	if (spaceship->GetLifes() <= 0) 
	{
		sceneManager->SetActualScene(Scenes::endGame);
	}
}
void Game::DrawUI() 
{
	DrawText("P(pause)", (GetScreenWidth() / 10), (GetScreenHeight() / 10), 20, WHITE);
	DrawText("ESC(exit)", (GetScreenWidth() / 10) * 8, (GetScreenHeight() / 10), 20, WHITE);

	DrawLifes();
}
void Game::Cursor() 
{
	if (!IsCursorHidden()) 
	{
		HideCursor();
	}
}
void Game::DrawLifes() 
{
	switch (spaceship->GetLifes())
	{
	case 1:
		DrawText("1", GetScreenWidth() / 2, GetScreenHeight() / 2, 60, WHITE);
		break;
	case 2:
		DrawText("2", GetScreenWidth() / 2, GetScreenHeight() / 2, 60, WHITE);
		break;
	case 3:
		DrawText("3", GetScreenWidth() / 2, GetScreenHeight() / 2, 60, WHITE);
		break;
	}
}
bool Game::allAsteroidsDestroy() 
{
	bool allDestoy = true;

	for (short i = 0; i < maxAsteroids; i++) 
	{
		if (asteroidsSpawner->GetAsteroid(i) != NULL) 
		{
			allDestoy = false;
			i = maxAsteroids;
		}
	}

	return allDestoy;
}
void Game::RestrictTheMouseMaxReach()
{
	if (GetMousePosition().x > GetScreenWidth()) 
	{
		SetMousePosition(GetScreenWidth(), GetMousePosition().y);
	}

	if (GetMousePosition().x < 0)
	{
		SetMousePosition(0, GetMousePosition().y);
	}

	if (GetMousePosition().y > GetScreenHeight())
	{
		SetMousePosition(GetMousePosition().x, GetScreenHeight());
	}

	if (GetMousePosition().y < 0)
	{
		SetMousePosition(GetMousePosition().x, 0);
	}
}