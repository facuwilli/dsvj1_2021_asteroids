#include "asteroid.h"

Asteroid::Asteroid(Vector2 center, Vector2 velocity, float radius, Color color)
{
	SetCenter(center);
	SetVelocity(velocity);
	SetRadius(radius);
	SetColor(color);
	touchingEdgeX = false;
	touchingEdgeY = false;
}
Asteroid::Asteroid()
{
	SetCenter({0.0f, 0.0f});
	SetVelocity({ 0.0f , 0.0f});
	SetRadius(0.0f);
	SetColor(asteroidColor);
	touchingEdgeX = false;
	touchingEdgeY = false;
}

void Asteroid::SetCenter(Vector2 center) 
{
	this->center = center;
}
void Asteroid::SetVelocity(Vector2 velocity)
{
	this->velocity = velocity;
}
void Asteroid::SetRadius(float radius) 
{
	this->radius = radius;
}
void Asteroid::SetColor(Color color) 
{
	this->color = color;
}

Vector2 Asteroid::GetCenter() 
{
	return center;
}
Vector2 Asteroid::GetVelocity()
{
	return velocity;
}
float Asteroid::GetRadius() 
{
	return radius;
}
Color Asteroid::GetColor() 
{
	return color;
}

void Asteroid::UpdateAsteroid()
{
	Move();
	BounceWhenTouchEdge();
}
void Asteroid::DrawAsteroid() 
{
	DrawCircleV(center, radius, color);
}

void Asteroid::Move()
{
	center.x += velocity.x * GetFrameTime();
	center.y += velocity.y * GetFrameTime();
}
void Asteroid::BounceWhenTouchEdge()
{
	if (GetCenter().x + GetRadius() >= GetScreenWidth() || GetCenter().x - GetRadius() <= 0)
	{
		if (!touchingEdgeX) 
		{
			velocity.x *= -1;
			touchingEdgeX = true;
		}		
	}
	else 
	{
		touchingEdgeX = false;
	}

	if (GetCenter().y + GetRadius() >= GetScreenHeight() || GetCenter().y - GetRadius() <= 0)
	{
		if (!touchingEdgeY) 
		{
			velocity.y *= -1;
			touchingEdgeY = true;
		}		
	}
	else 
	{
		touchingEdgeY = false;
	}
}
bool Asteroid::AsteroidBreakTheGame() 
{	
	return (GetCenter().x - GetRadius() >= GetScreenWidth() || GetCenter().x + GetRadius() <= 0
		|| GetCenter().y - GetRadius() >= GetScreenHeight() || GetCenter().y + GetRadius() <= 0);
}