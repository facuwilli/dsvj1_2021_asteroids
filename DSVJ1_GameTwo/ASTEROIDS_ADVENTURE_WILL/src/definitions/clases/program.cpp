#include "program.h"

Program::Program()
{
	InitWindow(screenWidth, screenHeight, title);
	SetTargetFPS(60);

	mainMenuInputScenes[0] = Scenes::game;
	mainMenuInputScenes[1] = Scenes::tutorial;
	mainMenuInputScenes[2] = Scenes::config;
	mainMenuInputScenes[3] = Scenes::credits;
	mainMenuInputScenes[4] = Scenes::exit;

	pauseMenuInputScenes[0] = Scenes::game;
	pauseMenuInputScenes[1] = Scenes::reset;
	pauseMenuInputScenes[2] = Scenes::config;
	pauseMenuInputScenes[3] = Scenes::gameToMenu;
	pauseMenuInputScenes[4] = Scenes::exit;
	
	mainMenuTexts[0] = "PLAY";
	mainMenuTexts[1] = "TUTORIAL";
	mainMenuTexts[2] = "CONFIG";
	mainMenuTexts[3] = "CREDITS";
	mainMenuTexts[4] = "EXIT";

	pauseMenuTexts[0] = "RESUME";
	pauseMenuTexts[1] = "RESET";
	pauseMenuTexts[2] = "CONFIG";
	pauseMenuTexts[3] = "MAIN MENU";
	pauseMenuTexts[4] = "EXIT";

	SetExit(false);
}
Program::~Program()
{
	CloseWindow();
}

void Program::SetExit(bool exit) 
{
	this->exit = exit;
}
bool Program::GetExit() 
{
	return exit;
}

void Program::Init()
{
	sceneManager = new SceneManager();
	game = new Game();
	mainMenu = new Menu(mainMenuTexts);
	pauseMenu = new Menu(pauseMenuTexts);
	credits = new Credits();
	tutorial = new Tutorial();
	endGame = new EndGame();
	config = new Config();
}
void Program::DeInit()
{
	delete sceneManager;
	delete game;
	delete mainMenu;	
	delete pauseMenu;
	delete credits;
	delete tutorial;
	delete endGame;
	delete config;
}
void Program::Run()
{
	Init();

	while (!WindowShouldClose() && !exit)
	{
		ScenesSwitch();
	}

	DeInit();
}

void Program::ScenesSwitch()
{
	switch (sceneManager->GetActualScene())
	{
	case Scenes::mainMenu:		

		mainMenu->InputMenu(sceneManager, mainMenuInputScenes);
		mainMenu->UpadateMenu(recButtonColor, textButtonColor);
		mainMenu->DrawMenu();
		
		break;
	case Scenes::game:

		game->GameLoop(sceneManager);
		
		break;
	case Scenes::exit:

		SetExit(true);
		
		break;
	case Scenes::pauseMenu:		
		
		pauseMenu->InputMenu(sceneManager, pauseMenuInputScenes);
		pauseMenu->UpadateMenu(recButtonColor, textButtonColor);
		pauseMenu->DrawMenu();

		break;
	case Scenes::credits:

		credits->Input(sceneManager, Scenes::mainMenu);
		credits->Update();
		credits->Draw();

		break;
	case Scenes::tutorial:

		tutorial->Input(sceneManager, Scenes::mainMenu);
		tutorial->Update();
		tutorial->Draw();

		break;
	case Scenes::endGame:
		
		endGame->Input(sceneManager, Scenes::mainMenu);
		endGame->Update(game);
		endGame->Draw(game);

		break;
	case Scenes::reset:
		
		game->ResetGame();
		sceneManager->SetActualScene(Scenes::game);
		
		break;
	case Scenes::gameToMenu:

		game->ResetGame();
		sceneManager->SetActualScene(Scenes::mainMenu);

		break;

	case Scenes::config:

		config->Input(sceneManager, Scenes::mainMenu);
		config->Update();
		config->Draw();

		break;
	default:

		SetExit(true);

		break;
	}
}