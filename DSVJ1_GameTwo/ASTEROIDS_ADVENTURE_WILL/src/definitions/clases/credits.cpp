#include "credits.h"

Credits::Credits() 
{
	goMainMenuButton = new Button();
	
	SetButtonSize();

	goMainMenuButton->SetText("MENU");
}
Credits::~Credits() 
{
	delete goMainMenuButton;
}

void Credits::Input(SceneManager* sceneManager, Scenes scene)
{
	goMainMenuButton->ChangeSceneWhenButtonPressed(sceneManager, scene);
}
void Credits::Update() 
{
	goMainMenuButton->ChangeColorWhenMouseOnButton(recButtonColor, textButtonColor, textButtonColor, recButtonColor);
}
void Credits::Draw() 
{
	BeginDrawing();

	ClearBackground(creditsBackgroundColor);

	DrawCredits();
	goMainMenuButton->DrawButton();	

	EndDrawing();
}

void Credits::SetButtonSize() 
{
	goMainMenuButton->SetStructureWidth(GetScreenWidth() / bigButtonWidthDivision);
	goMainMenuButton->SetStructureHeight(GetScreenHeight() / bigButtonHeightDivision);
	goMainMenuButton->SetStructureX((GetScreenWidth() / 2.0f) - (goMainMenuButton->GetStructure().width / 2.0f));
	goMainMenuButton->SetStructureY(goMainMenuButton->GetStructure().height * 6);
}
void Credits::DrawCredits() 
{
	DrawText("CREDITS", static_cast<int>((GetScreenWidth() / 2) - (creditsTitleFontSize * 3.5f)), (GetScreenHeight() / creditsTextSizeDivisions) * 2, creditsTitleFontSize, WHITE);
	DrawText("GAME DEVELOPED BY WILLIAMS FACUNDO", (GetScreenWidth() / 20) * 2, (GetScreenHeight() / creditsTextSizeDivisions) * 5, creditsTextFontSize, WHITE);
}