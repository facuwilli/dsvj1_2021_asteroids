#include "asteroids_spawner.h"

AsteroidsSpawner::AsteroidsSpawner() 
{
	for (short i = 0; i < maxAsteroids; i++) 
	{
		asteroids[i] = NULL;
	}

	bigAsteroids = 0;
	mediumAsteroids = 0;
	smallAsteroids = 0;
	tinyAsteroids = 0;
	
	CreateAsteroids(maxAsteroids);
}
AsteroidsSpawner::~AsteroidsSpawner() 
{
	DestroyAsteroids();
}

Asteroid* AsteroidsSpawner::GetAsteroid(short index)
{
	return asteroids[index];
}
void AsteroidsSpawner::DeleteAsteroid(short index)
{
	if (asteroids[index] != NULL)
	{
		delete asteroids[index];
		asteroids[index] = NULL;
	}
}

void AsteroidsSpawner::Update()
{
	for (short i = 0; i < maxAsteroids; i++)
	{
		if (asteroids[i] != NULL)
		{
			asteroids[i]->UpdateAsteroid();

			if (asteroids[i]->AsteroidBreakTheGame()) 
			{
				DeleteAsteroid(i);
			}
		}
	}
}
void AsteroidsSpawner::Draw()
{
	for (short i = 0; i < maxAsteroids; i++)
	{
		if (asteroids[i] != NULL)
		{
			asteroids[i]->DrawAsteroid();
		}
	}
}

void AsteroidsSpawner::CreateAsteroids(short amountAsteroids)
{
	bigAsteroids = 0;
	mediumAsteroids = 0;
	smallAsteroids = 0;
	tinyAsteroids = 0;

	//Definir cuantos asteroides de cada tipo gererar tomando en cuanta el maximo de asteroides
	RandomAmountOfEachTypeAsteroids(amountAsteroids);

	//Calcular el tama�o del asteroide dependiendo su tipo y el tama�o de la pantalla
	SetAsteroidsSize();

	//Darle una posicion random al asteroide que este por afuera de un radio donde aparece el player (centro de la pantalla)
	SetAsteroidsRandomPosition();

	//Darles una direccion y velocidad random a cada asteroide
	SetAsteroidsRandomDirectionAndVelocity();
}
void  AsteroidsSpawner::RandomAmountOfEachTypeAsteroids(short amountAsteroids)
{
	//Random para ver que generar de los 4 tipos teniendo en cuenta el limite maximo	
	while (amountAsteroids > 0)
	{
		if (amountAsteroids >= 15)
		{
			switch (GetRandomValue(1, 4))
			{
			case 1:
				tinyAsteroids += 1;
				amountAsteroids -= 1;
				break;
			case 2:
				smallAsteroids += 3;
				amountAsteroids -= 3;
				break;
			case 3:
				mediumAsteroids += 7;
				amountAsteroids -= 7;
				break;
			case 4:
				bigAsteroids += 15;
				amountAsteroids -= 15;
				break;
			default:
				tinyAsteroids += 1;
				amountAsteroids -= 1;
				break;
			}
		}
		else if (amountAsteroids >= 7)
		{
			switch (GetRandomValue(1, 3))
			{
			case 1:
				tinyAsteroids += 1;
				amountAsteroids -= 1;
				break;
			case 2:
				smallAsteroids += 3;
				amountAsteroids -= 3;
				break;
			case 3:
				mediumAsteroids += 7;
				amountAsteroids -= 7;
				break;			
			default:
				tinyAsteroids += 1;
				amountAsteroids -= 1;
				break;
			}
		}
		else if (amountAsteroids >= 3)
		{
			switch (GetRandomValue(1, 2))
			{
			case 1:
				tinyAsteroids += 1;
				amountAsteroids -= 1;
				break;
			case 2:
				smallAsteroids += 3;
				amountAsteroids -= 3;
				break;			
			default:
				tinyAsteroids += 1;
				amountAsteroids -= 1;
				break;
			}
		}
		else
		{
			tinyAsteroids += 1;
			amountAsteroids -= 1;
		}
	}	
}
void AsteroidsSpawner::SetAsteroidsSize()
{
	short amountAsteroids =	bigAsteroids + mediumAsteroids + smallAsteroids + tinyAsteroids;
	double screenSize = sqrt(pow(GetScreenWidth(), 2) + pow(GetScreenHeight(), 2));

	for (short i = 0; i < amountAsteroids; i++) 
	{
		asteroids[i] = new Asteroid();
	}

	amountAsteroids = 0;

	for (short i = amountAsteroids; i < amountAsteroids + bigAsteroids; i++)
	{
		asteroids[i]->SetRadius(static_cast<float>(screenSize / bigAsteroidsScreenDivision));
	}

	amountAsteroids += bigAsteroids;

	for (short i = amountAsteroids; i < amountAsteroids + mediumAsteroids; i++)
	{
		asteroids[i]->SetRadius(static_cast<float>(screenSize / mediumAsteroidsScreenDivision));
	}

	amountAsteroids += mediumAsteroids;

	for (short i = amountAsteroids; i < amountAsteroids + smallAsteroids; i++)
	{
		asteroids[i]->SetRadius(static_cast<float>(screenSize / smallAsteroidsScreenDivision));
	}

	amountAsteroids += smallAsteroids;

	for (short i = amountAsteroids; i < amountAsteroids + tinyAsteroids; i++)
	{
		asteroids[i]->SetRadius(static_cast<float>(screenSize / tinyAsteroidsScreenDivision));
	}
}
void AsteroidsSpawner::SetAsteroidsRandomPosition()
{
	int xPos = GetRandomValue(1, GetScreenWidth());
	int yPos = GetRandomValue(1, GetScreenHeight());
	short amountAsteroids = bigAsteroids + mediumAsteroids + smallAsteroids + tinyAsteroids;

	for (short i = 0; i < amountAsteroids; i++)
	{
		xPos = GetRandomValue(1, GetScreenWidth());
		yPos = GetRandomValue(1, GetScreenHeight());

		//The asteroid x position cant be inside the safe space
		while (xPos > (GetScreenWidth() / 2) - spaceshipSafeZoneX && xPos < (GetScreenWidth() / 2) + spaceshipSafeZoneX)
		{
			xPos = GetRandomValue(1, GetScreenWidth());			
		}

		while (yPos > (GetScreenHeight() / 2) - spaceshipSafeZoneY && yPos < (GetScreenHeight() / 2) + spaceshipSafeZoneY)
		{
			yPos = GetRandomValue(1, GetScreenHeight());
		}

		asteroids[i]->SetCenter({static_cast<float>(xPos),static_cast<float>(yPos)});
	}
}
void AsteroidsSpawner::SetAsteroidsRandomDirectionAndVelocity()
{
	short amountAsteroids = bigAsteroids + mediumAsteroids + smallAsteroids + tinyAsteroids;
	int xVel = 0;
	int yVel = 0;
	short direction = 0;

	for (short i = 0; i < amountAsteroids; i++)
	{
		xVel = GetRandomValue(static_cast<int>(asteroidsMinVelocity.x), static_cast<int>(asteroidsMaxVelocity.x));
		yVel = GetRandomValue(static_cast<int>(asteroidsMinVelocity.y), static_cast<int>(asteroidsMaxVelocity.y));

		direction = GetRandomValue(1, 4);

		switch (direction)
		{
		case 1:
			yVel *= -1;
			xVel *= 1;
			break;
		case 2:
			xVel *= -1;
			yVel *= -1;
			break;
		case 3:
			xVel *= -1;
			yVel *= 1;
			break;
		case 4:
			xVel *= 1;
			yVel *= 1;
			break;
		}

		asteroids[i]->SetVelocity({ static_cast<float>(xVel), static_cast<float>(yVel) });
	}
}
void AsteroidsSpawner::DestroyAsteroids()
{
	for (short i = 0; i < maxAsteroids; i++)
	{
		DeleteAsteroid(i);
	}
}