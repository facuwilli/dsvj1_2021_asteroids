#ifndef CREDITS_H
#define CREDITS_H

#include "button.h"

class Credits 
{
private:
	Button* goMainMenuButton;	
public:
	Credits();
	~Credits();

	void Input(SceneManager* sceneManager, Scenes scene);
	void Update();
	void Draw();

	void SetButtonSize();
	void DrawCredits();
};

#endif
