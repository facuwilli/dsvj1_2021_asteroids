#ifndef PROGRAM_H
#define PROGRAM_H

#include "game.h"
#include "menu.h"
#include "credits.h"
#include "tutorial.h"
#include "endGame.h"
#include "config.h"

class Program
{
private:
	SceneManager* sceneManager;
	Game* game;
	Menu* mainMenu;
	Menu* pauseMenu;
	Credits* credits;
	Tutorial* tutorial;
	EndGame* endGame;
	Config* config;
	
	Scenes mainMenuInputScenes[maxButtonsInMenu];
	Scenes pauseMenuInputScenes[maxButtonsInMenu];
	const char* mainMenuTexts[maxButtonsInMenu];
	const char* pauseMenuTexts[maxButtonsInMenu];

	bool exit;
public:
	Program();
	~Program();

	void SetExit(bool exit);
	bool GetExit();

	void Init();
	void DeInit();
	void Run();

	void ScenesSwitch();
};

#endif
