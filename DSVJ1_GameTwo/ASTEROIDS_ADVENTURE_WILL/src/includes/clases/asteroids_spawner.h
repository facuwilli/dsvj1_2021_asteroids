#ifndef ASTEROIDS_SPAWNER_H
#define ASTEROIDS_SPAWNER_H

#include "asteroid.h"

class AsteroidsSpawner 
{
private:	
	Asteroid* asteroids[maxAsteroids];
	short bigAsteroids;
	short mediumAsteroids;
	short smallAsteroids;
	short tinyAsteroids;
public:
	AsteroidsSpawner();
	~AsteroidsSpawner();

	Asteroid* GetAsteroid(short index);
	void DeleteAsteroid(short index);

	void Update();
	void Draw();

	void CreateAsteroids(short amountAsteroids);
	void RandomAmountOfEachTypeAsteroids(short amountAsteroids);
	void SetAsteroidsSize();
	void SetAsteroidsRandomPosition();
	void SetAsteroidsRandomDirectionAndVelocity();
	void DestroyAsteroids();	
};

#endif
