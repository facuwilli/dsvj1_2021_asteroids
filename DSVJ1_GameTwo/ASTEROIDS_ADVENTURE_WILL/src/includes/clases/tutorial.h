#ifndef TUTORIAL_H
#define TUTORIAL_H

#include "button.h"

class Tutorial 
{
private:
	Button* goMainMenuButton;
public:
	Tutorial();
	~Tutorial();

	void Input(SceneManager* sceneManager, Scenes scene);
	void Update();
	void Draw();

	void SetButtonSize();
	void DrawTutorial();
};

#endif