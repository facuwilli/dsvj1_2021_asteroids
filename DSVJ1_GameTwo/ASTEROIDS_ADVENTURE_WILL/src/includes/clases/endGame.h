#ifndef ENDGAME_H
#define ENDGAME_H

#include "button.h"
#include "game.h"

class EndGame 
{
private:
	Button* continueButton;
public:
	EndGame();
	~EndGame();

	void Input(SceneManager* sceneManager, Scenes scene);
	void Update(Game* game);
	void Draw(Game* game);

	void SetButtonSize();
	void Cursor();
};

#endif
