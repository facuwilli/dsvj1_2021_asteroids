#ifndef SPACESHIP_H
#define SPACESHIP_H

#include "asteroids_spawner.h"

class Spaceship 
{
private:
	//Spaceship
	Vector2 center;	
	Vector2 distance;
	int sides;
	float radius;
	float rotation;	
	Color color;
	short lifes;
	
	SpaceshipStatus status;
	Vector2 velocity;	

	//Bullet
	Bullet* bullets[maxBullet];	
public:
	Spaceship();
	~Spaceship();

	void SetCenter(Vector2 center);
	void SetSides(int sides);
	void SetRadius(float radius);
	void SetRotation(float rotation);
	void SetColor(Color color);	
	void SetStatus(SpaceshipStatus status);	
	void SetVelocity(Vector2 velocity);	
	void SetDistance(Vector2 distance);
	void DeleteBullet(short index);
	void SetLifes(short lifes);	

	Vector2 GetCenter();
	int GetSides();
	float GetRadius();
	float GetRotation();
	Color GetColor();	
	SpaceshipStatus GetStatus();
	Vector2 GetVelocity();
	Vector2 GetDistance();
	Bullet* GetBullet(short index);
	short GetLifes();

	void Input();
	void Update();
	void Draw();

	void DrawSpaceship();	
	void RotateSpaceshipDependingOnMousePosition();	
	void MoveSpaceship();
	void ResetSpaceship();
	void ResetSpaceshipWithSameLifes();
	void UpdateDistance();	

	void BulletsInput();
	void BulletsUpdate();
	void BulletsDraw();
	void ResetBullets();
};

#endif
