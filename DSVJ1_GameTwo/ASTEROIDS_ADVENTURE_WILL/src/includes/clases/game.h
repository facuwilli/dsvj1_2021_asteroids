#ifndef GAME_H
#define GAME_H

#include "scene_manager.h"
#include "spaceship.h"

class Game 
{
private:
	Spaceship* spaceship; 
	AsteroidsSpawner* asteroidsSpawner; 
	char score[maxScore];
public:
	Game();
	~Game();
	
	void Input(SceneManager* sceneManager);
	void Update(SceneManager* sceneManager);
	void Draw();
	void GameLoop(SceneManager* sceneManager);
	void ResetGame();

	void PauseGame(SceneManager* sceneManager);	
	void AsteroidBulletCollision();
	void ResetScore();
	void ScoreUp();
	void DrawScore();
	void DrawCursorPoint();
	void SpaceshipAsteroidsCollision();
	void LoseCondition(SceneManager* sceneManager);
	void DrawUI();
	void Cursor();
	void DrawLifes();
	bool allAsteroidsDestroy();
	void RestrictTheMouseMaxReach();
};

#endif

