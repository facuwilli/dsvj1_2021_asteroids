#ifndef CONFIG_H
#define CONFIG_H

#include "button.h"

class Config 
{
private:
	Button* goMainMenuButton;
public:
	Config();
	~Config();

	void Input(SceneManager* sceneManager, Scenes scene);
	void Update();
	void Draw();

	void SetButtonSize();
};

#endif
