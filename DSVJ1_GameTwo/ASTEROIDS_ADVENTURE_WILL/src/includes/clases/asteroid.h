#ifndef  ASTEROID_H
#define ASTEROID_H

#include "bullet.h"

class Asteroid 
{
private:
	Vector2 center;
	Vector2 velocity;
	float radius;
	Color color;
	bool touchingEdgeX;
	bool touchingEdgeY;
public:
	Asteroid(Vector2 center, Vector2 velocity, float radius, Color color);
	Asteroid();		

	void SetCenter(Vector2 center);
	void SetVelocity(Vector2 velocity);
	void SetRadius(float radius);
	void SetColor(Color color);

	Vector2 GetCenter();
	Vector2 GetVelocity();
	float GetRadius();
	Color GetColor();

	void UpdateAsteroid();
	void DrawAsteroid();

	void Move();
	void BounceWhenTouchEdge();
	bool AsteroidBreakTheGame();
};
#endif
