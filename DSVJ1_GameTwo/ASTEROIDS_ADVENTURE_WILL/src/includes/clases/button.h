#ifndef  BUTTON_H
#define BUTTON_H

#include "scene_manager.h"

class Button
{
private:
	Rectangle structure;
	Color recColor;
	Color textColor;
	const char* text;
public:
	Button(Rectangle structure, const char* text, Color recColor, Color textColor);
	Button(Rectangle structure);
	Button();

	void SetStructure(Rectangle structure);
	void SetStructure(float x, float y, float width, float height);
	void SetStructureX(float x);
	void SetStructureY(float y);
	void SetStructureWidth(float width);
	void SetStructureHeight(float height);
	void SetRecColor(Color recColor);
	void SetTextColor(Color textColor);
	void SetText(const char* text);

	Rectangle GetStructure();
	Color GetRecColor();
	Color GetTextColor();
	const char* GetText();

	bool IsMouseOnButton();
	bool ButtonPressed();	
	void ChangeColorWhenMouseOnButton(Color recButtonColor, Color recTextColor, Color recButtonColorTwo, Color recTextColorTwo);
	void ChangeSceneWhenButtonPressed(SceneManager* sceneManager, Scenes newScene);
	void DrawButton();	
};

#endif // ! BUTTON_H#pragma once
