#ifndef SCENE_MANAGER_H
#define SCENE_MANAGER_H

#include "variables.h"

class SceneManager
{
private:
	Scenes actualScene;
public:
	SceneManager();

	void SetActualScene(Scenes actualScene);
	Scenes GetActualScene();
};
#endif
