#ifndef MENU_H
#define MENU_H

#include "button.h"

class Menu 
{
private:
	Button* buttons[maxButtonsInMenu];	
public:	
	Menu(const char* texts[5]);
	~Menu();	

	void SetButtonsStructure();	

	void InputMenu(SceneManager* sceneManager, Scenes scenes[maxButtonsInMenu]);
	void UpadateMenu(Color recButtonColor, Color textButtonColor);
	void DrawMenu();
	void Cursor();
};

#endif