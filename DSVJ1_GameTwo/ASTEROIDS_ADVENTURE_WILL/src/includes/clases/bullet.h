#ifndef BULLET_H
#define BULLET_H

#include "variables.h"

class Bullet 
{
private:
	Vector2 position;
	Vector2 distance;	
	Vector2 velocity;
	Color color;
public:
	Bullet(Vector2 position, Vector2 distance);
		
	void SetPosition(Vector2 position);	
	void SetDistance(Vector2 distance);
	void SetColor(Color color);
	void SetVelocity(Vector2 velocity);

	Vector2 GetPosition();	
	Vector2 GetDistance();
	Color GetColor();
	Vector2 GetVelocity();

	void DrawBullet();	
	void MoveBullet();
	bool IsBulletInsideMap();	
};

#endif
