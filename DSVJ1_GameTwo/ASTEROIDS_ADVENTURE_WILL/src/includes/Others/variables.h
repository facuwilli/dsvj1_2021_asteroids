#ifndef VARIABLES_H
#define VARIABLES_H

#include <cmath>
#include "raylib.h"

//Screen 
const int screenWidth = 720;
const int screenHeight = 480;
static const char* title = "GAME";

//Scenes
enum class Scenes { mainMenu, game, exit, pauseMenu, credits, tutorial, endGame, reset, gameToMenu, config };
const Scenes startingScene = Scenes::mainMenu;

//Spaceship
const int spaceshipSides = 3;
const float spaceshipRadius = 20.0f;
const float spaceshipInitialRotation = 180.0f;
const Color spaceshipColor = GRAY;
const Color guidanceLineColor = GREEN;
enum class SpaceshipStatus {repose, moving};
const int moveSpaceshipButton = MOUSE_LEFT_BUTTON;
const short spaceshipSafeZoneX = 60;
const short spaceshipSafeZoneY = 60;
const short maxLifes = 3;

//Bullets 
const short maxBullet = 30;
const int shootBulletButton = MOUSE_RIGHT_BUTTON;
const Color bulletColor = RED;

//Asteroids
const short maxAsteroids = 30;
enum class Asteroids {big, medium, small, tiny};
const Color asteroidColor = GREEN;
const Vector2 asteroidsMinVelocity = { 20.0f, 20.0f };//20
const Vector2 asteroidsMaxVelocity = {120.0f, 120.0f};//100
const short firstLevel = 1;
const short bigAsteroidsScreenDivision = 15;
const short mediumAsteroidsScreenDivision = 22;
const short smallAsteroidsScreenDivision = 30;
const short tinyAsteroidsScreenDivision = 40;

//Menu (class)
const short maxButtonsInMenu = 5;
const float widthDivisionNum = 3.0f;
const float heightDivisionNum = static_cast<float>((maxButtonsInMenu + 1) * 2.0f);

//Main Menu
const Color recButtonColor = GREEN;
const Color textButtonColor = RED;
const Color mainMenubackgroundClearColor = BLACK;

//Game
const Color gameBackgroundColor = BLACK;
const short pauseGameKey = KEY_P;
const Color cursorPixelColor = YELLOW;

//Credits 
const Color creditsBackgroundColor = BLACK;
const short creditsTitleFontSize = 40;
const short creditsTextFontSize = 20;
const short creditsTextSizeDivisions = 20;

//Big Button
const float bigButtonWidthDivision = 3.0f;
const float bigButtonHeightDivision = 8.0f;

//Score
const short maxScore = 6;
const short scoreTextSize = 40;
const short scoreHeightDivision = 10;

//ASCII 
const char zero = 48;
const char nine = 57;


#endif
